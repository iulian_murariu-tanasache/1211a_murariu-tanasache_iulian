import java.io.BufferedReader
import java.io.InputStream
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

fun String.convertToDate(format : String) = LocalDate.parse(this, DateTimeFormatter.ofPattern(format))

fun main(args: Array<String>) {
    val data = readLine()
    println(data?.convertToDate("uuuu-MM-dd"))
}