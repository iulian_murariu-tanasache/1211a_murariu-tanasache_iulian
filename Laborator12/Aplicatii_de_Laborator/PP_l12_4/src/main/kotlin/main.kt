import kotlin.math.sqrt
import kotlin.properties.Delegates

fun Int.isPrime() : Boolean {
    if(this < 2)
        return false
    if(this == 2)
        return true
    for(i in sqrt(this.toDouble()).toInt() + 1 downTo 2)
        if(this % i == 0)
            return false
    return true
}

fun main(args: Array<String>) {
    var neaparatPrim : Int by Delegates.vetoable(2) {
        property, oldValue, newValue ->
        println("oldValue = $oldValue")
        println("newValue = $newValue")
        println()
        newValue.isPrime()
    }

    neaparatPrim = 3
    neaparatPrim = 4
    neaparatPrim = 5
    neaparatPrim = 6
}