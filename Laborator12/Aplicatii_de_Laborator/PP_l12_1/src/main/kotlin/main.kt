import java.util.*
import kotlin.math.sqrt

fun Int.isPrime() : Boolean {
    if(this < 2)
        return false
    if(this == 2)
        return true
    for(i in sqrt(this.toDouble()).toInt() + 1 downTo 2)
        if(this % i == 0)
            return false
    return true
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    var x = scan.nextInt()
    while(x > 0) {
        println("Este numarul prim? ${x.isPrime()}")
        x = scan.nextInt()
    }
}