fun main(args: Array<String>) {
    var input = readLine()
    var i = 0
    val mapa = mutableMapOf<String,Int>()
    while(input != ""){
        i++
        mapa[input!!] = i
        input = readLine()
    }
    println(mapa)
    val newMap = mapa.map{entry : Map.Entry<String,Int> -> entry.value to entry.key}.toMap()
    println(newMap)
}