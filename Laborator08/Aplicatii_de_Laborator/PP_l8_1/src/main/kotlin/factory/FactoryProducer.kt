package factory

class FactoryProducer {
    fun getFactory(choice: String): AbstractFactory {
        if(choice == "Elite")
            return EliteFactory()
        return HappyWorkerFactory()
    }
}