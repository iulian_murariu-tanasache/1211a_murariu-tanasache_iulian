package factory

import chain.CEOHandler
import chain.ExecutiveHandler

import chain.Handler
import chain.ManagerHandler

class EliteFactory: AbstractFactory() {
    override fun getHandler(handler: String): Handler {
        if(handler == "CEO")
            return CEOHandler()
        else if(handler == "Executive")
            return ExecutiveHandler()
        return ManagerHandler()
    }
}