package chain

class ExecutiveHandler(var next1: Handler? = null, var next2: Handler?
= null): Handler {
    override fun handleRequest(forwardDirection: String,
                               messageToBeProcessed: String) {
        println("Executive transmite $messageToBeProcessed")
        if(forwardDirection == "Right" || forwardDirection == "Start") {
            if(forwardDirection == "Start") {
                next1?.handleRequest("Right", "Oare e gata?")
                next2?.handleRequest("Right", messageToBeProcessed)
            }
            else
                next1?.handleRequest("Right", messageToBeProcessed)

        }
        else if(forwardDirection != "Right" || next1 == null)
            next2?.handleRequest("Right",messageToBeProcessed)
    }
}