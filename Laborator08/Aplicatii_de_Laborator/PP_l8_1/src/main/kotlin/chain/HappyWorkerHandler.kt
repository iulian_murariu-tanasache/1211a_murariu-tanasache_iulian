package chain

class HappyWorkerHandler(var next1: Handler? = null, var next2:
Handler? = null): Handler {
    override fun handleRequest(
        forwardDirection: String,
        messageToBeProcessed: String
    ) {
        if(messageToBeProcessed == "Oare e gata?")
        {
            println("Astept confirmare")
        }
        else if(messageToBeProcessed == "Done")
        {
            println("Done!")
        }
        else
        {
            println("Worker proceseaza $messageToBeProcessed")
            next2?.handleRequest("?","Done")
        }
    }
}