import chain.CEOHandler
import chain.ExecutiveHandler
import chain.HappyWorkerHandler
import chain.ManagerHandler
import factory.FactoryProducer

fun main(args: Array<String>) {
    // se creeaza 1xFactoryProducer, 1xEliteFactory, 1xHappyWorkerFactory
    val factoryProducer = FactoryProducer()
    val eliteFactory = factoryProducer.getFactory("Elite")
    val happyFactory = factoryProducer.getFactory("fericire")
    // crearea instantelor (prin intermediul celor 2 fabrici):
    // 2xCEOHandler, 2xExecutiveHandler, 2xManagerHandler, 2xHappyWorkerHandler
    val ceo1 = eliteFactory.getHandler("CEO") as CEOHandler
    val ceo2 = eliteFactory.getHandler("CEO") as CEOHandler
    val exec1 = eliteFactory.getHandler("Executive") as ExecutiveHandler
    val exec2 = eliteFactory.getHandler("Executive") as ExecutiveHandler
    val manager1 = eliteFactory.getHandler("manager") as ManagerHandler
    val manager2 = eliteFactory.getHandler("manager") as ManagerHandler
    val happyWorker1 = happyFactory.getHandler("happy") as HappyWorkerHandler
    val happyWorker2 = happyFactory.getHandler("happy") as HappyWorkerHandler
    // se construieste lantul (se verifica intai diagrama de obiecte si se realizeaza legaturile)
    //1 este right; 2 este up/down
    ceo1.next1 = exec1
    ceo1.next2 = ceo2
    ceo2.next1 = exec2
    ceo2.next2 = ceo1
    exec1.next1 = manager1
    exec1.next2 = exec2
    exec2.next1 = manager1
    exec2.next2 = exec1
    manager1.next1 = happyWorker1
    manager1.next2 = manager2
    manager2.next1 = happyWorker2
    manager2.next2 = manager1
    happyWorker1.next2 = happyWorker2
    happyWorker2.next2 = happyWorker1
    // se executa lantul utilizand atat mesaje de prioritate diferita, cat si directii diferite in lant
    ceo1.handleRequest("Start","Placinte")
    println()
    ceo2.handleRequest("Start","Paine")
    println()
    exec1.handleRequest("Start","Ciocolata")
}