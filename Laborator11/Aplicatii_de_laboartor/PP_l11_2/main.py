import random
import threading
import multiprocessing
from math import sqrt
from concurrent.futures import ThreadPoolExecutor
import time
from math import ceil

lista = []
dim = 5000


def sort(list):
    for i in range(0, len(list) - 1):
        for j in range(i + 1, len(list)):
            if list[i] > list[j]:
                aux = list[i]
                list[i] = list[j]
                list[j] = aux
    # for i in list:
    #     print(i, end=" ")


def prime_filter(list):
    newList = []
    for i in list:
        prime = True
        if i < 2:
            prime = False
        else:
            for j in range(2, ceil(sqrt(i)) + 1):
                if i % j == 0:
                    prime = False
                    break
        if prime:
            newList.append(i)
    list = newList


def square(list):
    newList = []
    for i in list:
        newList.append(i * i)
    list = newList


def ver_1():
    newSeg1 = []
    newSeg2 = []
    newSeg3 = []
    for i in range(0, int(dim / 3)):
        newSeg1.append(i)
    for i in range(int(dim / 3), int(2 * dim / 3)):
        newSeg2.append(i)
    for i in range(int(2 * dim / 3), dim):
        newSeg3.append(i)
    thread_1 = threading.Thread(target=sort, args=[newSeg1])
    thread_2 = threading.Thread(target=prime_filter, args=[newSeg2])
    thread_3 = threading.Thread(target=square, args=[newSeg3])
    thread_1.start()
    thread_2.start()
    thread_3.start()
    thread_1.join()
    thread_2.join()
    thread_3.join()


def ver_2():
    newSeg1 = []
    newSeg2 = []
    newSeg3 = []
    for i in range(0, int(dim / 3)):
        newSeg1.append(i)
    for i in range(int(dim / 3), int(2 * dim / 3)):
        newSeg2.append(i)
    for i in range(int(2 * dim / 3), dim):
        newSeg3.append(i)
    sort(newSeg1)
    prime_filter(newSeg2)
    square(newSeg3)


def ver_3():
    newSeg1 = []
    newSeg2 = []
    newSeg3 = []
    for i in range(0, int(dim / 3)):
        newSeg1.append(i)
    for i in range(int(dim / 3), int(2 * dim / 3)):
        newSeg2.append(i)
    for i in range(int(2 * dim / 3), dim):
        newSeg3.append(i)
    process_1 = multiprocessing.Process(target=sort, args=(newSeg1,))
    process_2 = multiprocessing.Process(target=prime_filter, args=(newSeg2,))
    process_3 = multiprocessing.Process(target=square, args=(newSeg3,))
    process_1.start()
    process_2.start()
    process_3.start()
    process_1.join()
    process_2.join()
    process_3.join()


def ver_4():
    newSeg1 = []
    newSeg2 = []
    newSeg3 = []
    for i in range(0, int(dim / 3)):
        newSeg1.append(i)
    for i in range(int(dim / 3), int(2 * dim / 3)):
        newSeg2.append(i)
    for i in range(int(2 * dim / 3), dim):
        newSeg3.append(i)
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(sort, newSeg1)
        future = executor.submit(prime_filter, newSeg2)
        future = executor.submit(square, newSeg3)


if __name__ == '__main__':
    random.seed(0)
    for i in range(0, dim):
        lista.append(random.randrange(2 * dim))
    start = time.time()
    ver_1()
    end = time.time()
    print("\n Timp executie pseudoparalelism cu GIL")
    print(end - start)
    start = time.time()
    ver_2()
    end = time.time()
    print("\n Timp executie secvential")
    print(end - start)
    start = time.time()
    ver_3()
    end = time.time()
    print("\n Timp executie paralela cu multiprocessing")
    print(end - start)
    start = time.time()
    ver_4()
    end = time.time()
    print("\n Timp executie paralela cu concurrent.futures")
    print(end - start)
