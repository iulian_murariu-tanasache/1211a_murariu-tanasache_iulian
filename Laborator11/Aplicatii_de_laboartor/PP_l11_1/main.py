import threading
import multiprocessing
from concurrent.futures import ThreadPoolExecutor
import time
import random
from math import sqrt, ceil

list = []
mySet = set()
dim = 5000


# def countdown():
#     newList = []
#     for i in list:
#         newList.append(i)
#     for i in range(0, dim - 1):
#         for j in range(i + 1, dim):
#             if newList[i] > newList[j]:
#                 aux = newList[i]
#                 newList[i] = newList[j]
#                 newList[j] = aux

def countdown():
    newSet = set()
    for i in mySet:
        prime = True
        if i < 2:
            prime = False
        else:
            for j in range(2, ceil(sqrt(i)) + 1):
                if i % j == 0:
                    prime = False
                    break
        if prime:
            newSet.add(i)

    # for i in newSet:
    #     print(i, end=" ")


def ver_1():
    thread_1 = threading.Thread(target=countdown)
    thread_2 = threading.Thread(target=countdown)
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()


def ver_2():
    countdown()
    countdown()


def ver_3():
    process_1 = multiprocessing.Process(target=countdown)
    process_2 = multiprocessing.Process(target=countdown)
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()


def ver_4():
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(countdown())
        future = executor.submit(countdown())


if __name__ == '__main__':
    random.seed(0)
    for i in range(0, dim):
        list.append(random.randrange(2 * dim))
        mySet.add(random.randrange(2 * dim))
    start = time.time()
    ver_1()
    end = time.time()
    print("\n Timp executie pseudoparalelism cu GIL")
    print(end - start)
    start = time.time()
    ver_2()
    end = time.time()
    print("\n Timp executie secvential")
    print(end - start)
    start = time.time()
    ver_3()
    end = time.time()
    print("\n Timp executie paralela cu multiprocessing")
    print(end - start)
    start = time.time()
    ver_4()
    end = time.time()
    print("\n Timp executie paralela cu concurrent.futures")
    print(end - start)
