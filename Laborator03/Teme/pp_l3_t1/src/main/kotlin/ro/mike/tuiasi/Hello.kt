package ro.mike.tuiasi

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import java.io.File
import java.io.IOException

class item(var title:String, var description: String, var link:String, var pubDate:String){
    fun get_title() = "$title"
    fun get_link() = "$link"
}

/**
 * @param source - string specifying the source type (url, file, string)
 * @param url - string containing an URL, a path to a HTML file or an HTML string
 * @param baseURI - string used for the relative links inside of a local HTML file
 * @throws Exception - if the source is unknown
 */
fun makeItem(url : String? = null) : item {
    var htmlDocument: Document? = null
    try {
        htmlDocument = Jsoup.connect(url).get()
    }
    catch (e: IOException)
    {
        e.printStackTrace()
    }

    val i = item(htmlDocument?.title().toString(),htmlDocument?.getElementsByAttributeValue("property","og:description")?.attr("content").toString(),htmlDocument?.getElementsByAttributeValue("property","og:url")?.attr("content").toString(),htmlDocument?.getElementsByAttributeValue("property","article:published_time")?.attr("content").toString())
    return i
}



fun main(args: Array<String>) {
    println("---------RSS FEED----------\n")
    val list = mutableListOf<item>()
    list.add(makeItem("https://www.ea.com/games/apex-legends/news"))
    list.add(makeItem("https://myanimelist.net/"))
    list.add(makeItem("https://www.historia.ro/"))
    list.add(makeItem("https://www.reddit.com/"))
    list.add(makeItem("https://www.miniclip.com/games/ro/"))
    list.add(makeItem("https://docs.microsoft.com/en-us/cpp/"))
    list.forEach { it:item ->
        println(it.get_title() + "  -> " + it.get_link())
    }
}

