package ro.mike.tuiasi

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import java.io.File
import java.io.IOException
import java.util.*

class Nod(var url:String)
{
    var fii = mutableListOf<Nod>()
}

/**
 * @param source - string specifying the source type (url, file, string)
 * @param url - string containing an URL, a path to a HTML file or an HTML string
 * @param baseURI - string used for the relative links inside of a local HTML file
 * @throws Exception - if the source is unknown
 */


fun makeTree(root : Nod, domain : String, _adancime : Int){
    var adancime = _adancime - 1
    if(adancime < 0)
        return
    val doc = Jsoup.connect(root.url).get()
    val links = doc.select("a[href]")
    links.forEach {
        var s:String = it.attr("abs:href")
        if(s.contains(domain)){
            root.fii.add(Nod(s))
            makeTree(root.fii.last(),domain,adancime)
        }
    }
}

fun parcurgere(root : Nod?)
{
    if(root == null)
        return
    var t : Nod
    val q: Queue<Nod> = LinkedList<Nod>()
    var enter = 0
    var nextEnter = root.fii.size
    q.add(root);
    println("[ " + root.url + " ]" )
    while (!q.isEmpty()) {
        t = q.poll()
        if(t.fii.size > 0){
            print("[ ")
            t.fii.forEach{
                print(it.url + ", ")
                nextEnter += it.fii.size
                q.add(it)
            }
            print("] ")
        }
        if (enter == 0)
        {
            println("\n")
            enter = nextEnter
            nextEnter = 0
        }
        else {
            enter--
        }
    }
}

fun serialize(file : String, root : Nod)
{
    var t : Nod
    val q: Queue<Nod> = LinkedList<Nod>()
    q.add(root);
    File(file).writeText(root.url+",\n")
    while (!q.isEmpty()) {
        t = q.poll()
        if(t.fii.size > 0){
            t.fii.forEach { it->
                File(file).appendText(it.url+",")
                q.add(it)
            }
        }
        else {
            File(file).appendText("-")
        }
        File(file).appendText("\n")
    }
}

fun deserialize(file : String) : Nod?
{
    var t : Nod
    val q: Queue<Nod> = LinkedList<Nod>()
    var root : Nod? = null
    File(file).forEachLine {
        if(root == null)
        {
            root = initializeTree(it.replace(",",""))
            q.add(root);
        }
        else
        {
            t = q.poll()
            if(it != "-")
            {
                val list = it.split(",")
                list.forEach { alt->
                        val nou = Nod(alt)
                        t.fii.add(nou)
                        q.add(nou)
                }
            }
        }
    }
    return root
}

fun initializeTree(url:String) = Nod(url)

fun main(args: Array<String>) {
    val root = initializeTree("https://stackoverflow.com/")
    makeTree(root,"https://stackoverflow.com/",2)
    serialize("serializare.out",root)
    val newRoot = deserialize("serializare.out")
    parcurgere(newRoot)
}

