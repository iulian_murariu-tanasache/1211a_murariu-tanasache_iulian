import java.io.File

abstract class Handler {
    var urmator : Handler? = null

    fun setNext(h: Handler) {
        if(urmator == null)
            urmator = h
    }
    abstract fun handle(file : File) : Command
}

class KotlinHandler() : Handler(){

    override fun handle(file: File): Command {
        var cmd: Command = CommandCreator("nu",file)
        var succes: Boolean = false
        file.forEachLine ceva@{
            if(it.contains("fun ") || it.contains("var ") || it.contains("val ")) {
                cmd = CommandCreator("Kotlin", file)
                succes = true
                return@ceva
            }
        }
        if(succes)
            return cmd
        return urmator?.handle(file) ?: cmd
    }

}

class PythonHandler() : Handler(){

    override fun handle(file: File): Command {
        var succes: Boolean = false
        var cmd: Command = CommandCreator("nu",file)
        file.forEachLine ceva@{
            if(it.contains("def ") || it.contains(" __init__") || it.contains("self") || it.contains(" __name__")) {
                cmd = CommandCreator("python", file)
                succes = true
                return@ceva
            }
        }
        if(succes)
            return cmd
        return urmator?.handle(file) ?: cmd
    }

}

class BashHandler() : Handler(){

    override fun handle(file: File): Command {
        var succes : Boolean = false
        var cmd: Command = CommandCreator("nu",file)
        file.forEachLine ceva@{
            if(it.contains("#!") || it.contains("echo ") || it.contains("eval ")) {
                cmd = CommandCreator("bash", file)
                succes = true
                return@ceva
            }
        }
        if(succes)
            return cmd
        return urmator?.handle(file) ?: cmd
    }

}

class JavaHandler() : Handler(){

    override fun handle(file: File): Command {
        var succes : Boolean = false
        var cmd: Command = CommandCreator("nu",file)
        file.forEachLine ceva@{
            if(it.contains("java") || it.contains("public static void main") || it.contains("package ")) {
                cmd = CommandCreator("java", file)
                succes = true
                return@ceva
            }
        }
        if(succes)
            return cmd
        return urmator?.handle(file) ?: cmd
    }

}

class NotKnownHandler() : Handler(){

    override fun handle(file: File): Command {
        println("Nu se poate executa acest fisier!")
        return CommandCreator("nu",file)
    }

}

fun CommandCreator(command : String, file : File) : Command {
    if(command == "Kotlin") return KotlinExec(file)
    if(command == "python") return PythonExec(file)
    if(command == "bash") return BashExec(file)
    if(command == "java") return JavaExec(file)
    return NoExec(file)
}

interface Command {
    val file : File
    fun execute()
}

class KotlinExec(override val file: File) : Command {

    override fun execute() {
        //val process1 = Runtime.getRuntime().exec("kotlinc ${file.name} -include-runtime -d ${file.name.substring(0,file.name.lastIndexOf( '.' ))}.jar")
        //val process2 = Runtime.getRuntime().exec("java -jar ${file.name.substring(0,file.name.lastIndexOf( '.' ))}.jar")
        ProcessBuilder("kotlinc", file.name, "-include-runtime","-d","${file.nameWithoutExtension}.jar")
            .directory(File(file.parent))
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .start()
            .waitFor()
        ProcessBuilder("java", "-jar", "${file.nameWithoutExtension}.jar")
            .directory(File(file.parent))
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .start()
            .waitFor()
    }
}

class PythonExec(override val file: File) : Command {

    override fun execute() {
        //val process1 = Runtime.getRuntime().exec("kotlinc ${file.name} -include-runtime -d ${file.name.substring(0,file.name.lastIndexOf( '.' ))}.jar")
        //val process2 = Runtime.getRuntime().exec("java -jar ${file.name.substring(0,file.name.lastIndexOf( '.' ))}.jar")
        //println(file.name)
        ProcessBuilder("python3", file.name)
            .directory(File(file.parent))
            .inheritIO()
            .start()
            .waitFor()
    }
}

class BashExec(override val file: File) : Command {

    override fun execute() {
        //val process1 = Runtime.getRuntime().exec("kotlinc ${file.name} -include-runtime -d ${file.name.substring(0,file.name.lastIndexOf( '.' ))}.jar")
        //val process2 = Runtime.getRuntime().exec("java -jar ${file.name.substring(0,file.name.lastIndexOf( '.' ))}.jar")
        //println(file.name)
        ProcessBuilder("./${file.name}")
            .directory(File(file.parent))
            .inheritIO()
            .start()
            .waitFor()
    }
}

class JavaExec(override val file: File) : Command {

    override fun execute() {
        //val process1 = Runtime.getRuntime().exec("kotlinc ${file.name} -include-runtime -d ${file.name.substring(0,file.name.lastIndexOf( '.' ))}.jar")
        //val process2 = Runtime.getRuntime().exec("java -jar ${file.name.substring(0,file.name.lastIndexOf( '.' ))}.jar")
        println(file.parent)
        ProcessBuilder("javac", file.name)
            .directory(File(file.parent))
            .inheritIO()
            .start()
            .waitFor()
        ProcessBuilder("java", "Main")
            .directory(File(file.parent))
            .inheritIO()
            .start()
            .waitFor()
    }
}



class NoExec(override val file: File) : Command {

    override fun execute() {
        println("Nu fac nimic!")
    }
}


fun main(args: Array<String>) {
    val kotlin = KotlinHandler()
    val bash = BashHandler()
    val python = PythonHandler()
    val java = JavaHandler()
    val unknown = NotKnownHandler()
    kotlin.setNext(python)
    python.setNext(bash)
    bash.setNext(java)
    java.setNext(unknown)
    val filePath = readLine()
    println("Sa se introduca path-ul complet al fisierului")
    val cmd = kotlin.handle(File(filePath))
    cmd.execute()
}