import copy
from abc import ABCMeta


class File(metaclass=ABCMeta):
    _title = ''
    _author = ''
    _num_par = 0
    _paragraphs = []

    def read_file_from_stdin(self):
        self._title = input('Title: ')
        self._author = input('Author: ')
        self._num_par = int(input('Number of paragraphs: '))
        for i in range(0, self._num_par):
            par = input('')
            self._paragraphs.append(par)


class FileFactory:
    def factory(self, file_type_str):
        if file_type_str == 'HTML':
            return HTMLFile()
        elif file_type_str == 'JSON':
            return JSONFile()
        elif file_type_str == 'Article':
            return ArticleTextFile()
        else:
            return BlogTextFile()


class HTMLFile(File):

    def __init__(self):
        self.read_file_from_stdin()
        self._title = '<title>' + self._title + '</title>'
        self._author = '<author>' + self._author + '</author>'
        para = []
        for par in self._paragraphs:
            par = '<paragraph>' + par + '</paragraph>'
            para.append(par)
        self._paragraphs = para

    def print_html(self):
        print('<html>')
        print(self._title)
        print(self._author)
        for par in self._paragraphs:
            print(par)
        print('</html>')


class JSONFile(File):
    def __init__(self):
        self.read_file_from_stdin()
        self._title = '\"title\": ' + self._title + ','
        self._author = '\"author\": ' + self._author + ','
        para = []
        for par in self._paragraphs:
            par = '\"' + par + '\",'
            para.append(par)
        self._paragraphs = para

    def print_json(self):
        print("{")
        print("\t" + self._title)
        print("\t" + self._author)
        print("\t\"paragraphs\": [", end='')
        for par in self._paragraphs:
            print(par, end=" ")
        print("]")
        print("}")


class TextFile(File, metaclass=ABCMeta):
    _template = ''

    def print_text(self):
        pass

    def clone(self):
        return copy.copy(self)


class ArticleTextFile(TextFile):

    def __init__(self):
        self.read_file_from_stdin()
        self._template = 'Article'

    def print_text(self):
        print('\t' + self._title)
        print('\t\t by ' + self._author)
        for par in self._paragraphs:
            print(par)


class BlogTextFile(TextFile):

    def __init__(self):
        self.read_file_from_stdin()
        self._template = 'Blog'

    def print_text(self):
        print(self._title)
        for par in self._paragraphs:
            print(par)
        print()
        print('Written by ' + self._author)


def main():
    factory = FileFactory()
    html = factory.factory('HTML')
    html.print_html()
    json = factory.factory('JSON')
    json.print_json()
    article = factory.factory('Article')
    article.print_text()
    #blog = BlogTextFile(TextFile(article).clone())



if __name__ == '__main__':
    main()
