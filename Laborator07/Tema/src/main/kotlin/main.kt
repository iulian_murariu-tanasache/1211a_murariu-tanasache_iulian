import java.io.File
import java.sql.Timestamp

fun <T: Comparable<T>> maxOfHistoryLog(a : T, b : T) : T {
    if(a.compareTo(b) >= 0)
        return a
    return b
}
class HistoryLogRecord (val timestamp : Timestamp, val command : String) : Comparable<HistoryLogRecord>
{
    override fun compareTo(other: HistoryLogRecord): Int {
        return this.timestamp.nanos - other.timestamp.nanos
    }

    override fun toString(): String {
        return "HistoryLogRecord(timestamp=$timestamp, command='$command')"
    }


}

fun creareLog(date : String, command : String) : HistoryLogRecord
{
    val year = date.substring(0,4).toInt() - 1900
    val month = date.substring(5,7).toInt()
    val day = date.substring(8,10).toInt()
    val hour = date.substring(12,14).toInt()
    val minute = date.substring(15,17).toInt()
    val sec = date.substring(18,20).toInt()
    val time = Timestamp(year,month,day,hour,minute,sec,0)
    return HistoryLogRecord(time,command)
}

fun procesare() : MutableMap<Timestamp,HistoryLogRecord>
{
    val map = mutableMapOf<Timestamp,HistoryLogRecord>()
    var records = 0
    var time : String? = null
    var command : String? = null
    File("history.log").forEachLine fin@{
        if(it == "\n")
            records++
        else
        {
            if(it.contains("Start-Date"))
            {
                time = it.substring(it.indexOf(":") + 2)
                //println(time)

            }
            else if(it.contains("Commandline"))
            {
                command = it.substring(it.indexOf(":") + 2)
                //println(command)
            }
            if(time != null && command != null)
            {
                val log = creareLog(time!!, command!!)
                map.put(log.timestamp,log)
                time = null
                command = null
            }
        }
        if(records >= 50)
            return@fin
    }
    return map
}

fun inlocuire(deInlocuit : HistoryLogRecord, inlocuitor : HistoryLogRecord, unde : MutableMap<Timestamp,HistoryLogRecord>){
    unde.remove(deInlocuit.timestamp,deInlocuit)
    unde.put(inlocuitor.timestamp, inlocuitor)
}


fun main(args: Array<String>) {
    val map = procesare()
    var max : HistoryLogRecord? = null
    map.forEach{
        if(max == null)
            max = it.value
        else
            max = maxOfHistoryLog(max!!,it.value)
    }
    println(max.toString())
    val newLog = creareLog("2011-11-11  11:11:11","comanda random sa para ca face ceva")
    max?.let { inlocuire(it,newLog,map) }
    map.forEach{
        println(it)
    }
}