import pygame
import multiprocessing as mp
from multiprocessing.managers import BaseManager
import threading
import sys

colors = {
    'white': (255, 255, 255),
    'lightgrey': (155, 155, 155),
    'grey': (105, 105, 105),
    'black': (0, 0, 0)
}


class read:

    def __init__(self):
        self.toRead = 1  # 1 - server; 2 - client; 0 - nimeni

    def setRead(self, i):
        self.toRead = i

    def getRead(self):
        return self.toRead


class Stuff:
    state = 0
    score1 = 0
    score2 = 0
    on = True
    hasServer = False
    queue = None
    manager = None
    myTurn = False
    toRead = None

    @classmethod
    def getScore(cls):
        return font.render("Score P1: " + str(cls.score1), 1, colors['black']), font.render(
            "Score P2: " + str(cls.score2), 1,
            colors['black'])


def play():
    if Stuff.state == 0:
        Stuff.state = 1


class Button:
    def __init__(self, x, y, w=0, h=0, color="", text=" ", textpos=(0, 0)):
        self.textpos = textpos
        self.x = x
        self.textname = text
        self.y = y
        self.h = h
        self.w = w
        self.color = color
        if color == '':
            self.color = 'white'

    def draw(self, window):
        pygame.draw.rect(window, colors[self.color], (self.x, self.y, self.w, self.h))
        text = font.render(f"{self.textname}", 1, colors['black'])
        window.blit(text, (self.x + self.textpos[0], self.y + self.textpos[1]))

    def isMouseOn(self):
        mouseX, mouseY = pygame.mouse.get_pos()
        if self.x <= mouseX <= self.x + self.w and self.y <= mouseY <= self.y + self.h:
            return True
        return False


class DeAiaDePuiPeTabla(Button):
    def __init__(self, x, y, w=0, h=0, color="", text="", textpos=(0, 0)):
        super().__init__(x, y, w, h, color, text, textpos)
        self.who = 0

    def isMouseOn(self, player):
        mouseX, mouseY = pygame.mouse.get_pos()
        if self.x <= mouseX <= self.x + self.w and self.y <= mouseY <= self.y + self.h:
            if self.who == 0:
                self.who = player
                if player == 1:
                    self.textname = "O"
                elif player == 2:
                    self.textname = "X"
                return True
        return False

    def draw(self, window):
        pygame.draw.rect(window, colors[self.color], (self.x, self.y, self.w, self.h))
        text = playFont.render(f"{self.textname}", 1, colors['black'])
        window.blit(text, (self.x + self.textpos[0], self.y + self.textpos[1]))


class Board:
    turn = 1

    def __init__(self, x, y, w, h):
        self.clicked = 0
        self.x = x
        self.y = y
        self.h = h
        self.w = w
        self.list = []
        for i in range(0, 3):
            for j in range(0, 3):
                self.list.append(DeAiaDePuiPeTabla(x + w * j, y + h * i, w, h, 'lightgrey', "", (10, 10)))

    def draw(self, window):
        for desen in self.list:
            desen.draw(window)
        pygame.draw.line(window, colors['black'], (self.x + self.w, self.y), (self.x + self.w, self.y + self.h * 3))
        pygame.draw.line(window, colors['black'], (self.x + self.w * 2, self.y),
                         (self.x + self.w * 2, self.y + self.h * 3))
        pygame.draw.line(window, colors['black'], (self.x, self.y + self.h), (self.x + self.w * 3, self.y + self.h))
        pygame.draw.line(window, colors['black'], (self.x, self.y + self.h * 2),
                         (self.x + self.w * 3, self.y + self.h * 2))

    def click(self):
        isClicked = False
        if Stuff.state == 1:
            for desen in self.list:
                if desen.isMouseOn(self.turn):
                    self.clicked = self.clicked + 1
                    isClicked = True

        return isClicked

    def checkWin(self):
        if self.list[0].who == self.list[1].who == self.list[2].who and self.list[0].who != 0:
            return self.list[2].who
        if self.list[3].who == self.list[4].who == self.list[5].who and self.list[3].who != 0:
            return self.list[3].who
        if self.list[6].who == self.list[7].who == self.list[8].who and self.list[6].who != 0:
            return self.list[6].who
        if self.list[0].who == self.list[3].who == self.list[6].who and self.list[0].who != 0:
            return self.list[0].who
        if self.list[1].who == self.list[4].who == self.list[7].who and self.list[1].who != 0:
            return self.list[1].who
        if self.list[2].who == self.list[5].who == self.list[8].who and self.list[2].who != 0:
            return self.list[2].who
        if self.list[0].who == self.list[4].who == self.list[8].who and self.list[0].who != 0:
            return self.list[0].who
        if self.list[2].who == self.list[4].who == self.list[6].who and self.list[2].who != 0:
            return self.list[2].who
        if self.clicked == 9:
            return -1
        return 0

    def reset(self):
        for ceva in self.list:
            ceva.who = 0
            ceva.textname = " "
        self.clicked = 0

    def fullReset(self):
        self.reset()
        Stuff.score1 = 0
        Stuff.score2 = 0


def updateSend(board):
    if Stuff.myTurn is False:
        return
    Stuff.myTurn = False
    for ceva in board.list:
        Stuff.queue.put(ceva.textname)


def updateGet(board):
    if Stuff.myTurn is False or Stuff.queue.empty():
        return

    board.clicked = 0
    for ceva in board.list:
        rez = Stuff.queue.get()
        if rez == "Connected":
            return
        elif rez == "Reset":
            board.fullReset()
            return
        ceva.textname = rez
        if rez == 'O':
            ceva.who = 1
            board.clicked = board.clicked + 1
        elif rez == 'X':
            ceva.who = 2
            board.clicked = board.clicked + 1


def main():
    clock = pygame.time.Clock()
    FPS = 60
    pause = 0
    win = 0
    Stuff.on = True
    winText = None
    board = Board(75, 75, 100, 100)
    waiting = font.render("Waiting for second player...", 1, colors['black'])
    playButton = Button(150, 200, 150, 50, 'lightgrey', "Play", (15, -2))
    resButton = Button(350, 10, 90, 40, 'lightgrey', "Reset", (5, 0))
    who = 0

    def drawWindow():
        window.fill(colors['white'])
        if Stuff.state == 0:
            window.blit(waiting, (50, 100))
            # playButton.draw(window)
        else:
            board.draw(window)
            textS1, textS2 = Stuff.getScore()
            window.blit(textS1, (10, 390))
            window.blit(textS2, (280, 390))
            turnText = ' You'
            if Stuff.myTurn is False:
                turnText = ' Other'
            window.blit(font.render("Turn:" + turnText, 1, colors['black']), (10, 10))
            resButton.draw(window)
            if pause > 0 and winText is not None:
                if win == -1:
                    window.blit(winText, (177, 200))
                else:
                    window.blit(winText, (125, 200))
        pygame.display.update()

    while Stuff.on:
        clock.tick(FPS)
        if Stuff.toRead is not None:
            who = Stuff.toRead.getRead()
        if Stuff.queue is not None and Stuff.queue.empty() is False and ((
                                                                                 who == 1 and Stuff.hasServer is True) or (
                                                                                 who == 2 and Stuff.hasServer is False)):
            if Stuff.state == 0:
                readQ = Stuff.queue.get()
                # input field
                if readQ == 'Connected':
                    Stuff.state = 1
                    if who == 1:
                        Stuff.toRead.setRead(2)
                    elif who == 2:
                        Stuff.toRead.setRead(1)
                        board.turn = 2
                    Stuff.queue.put('Connected')
            elif Stuff.state == 1:
                Stuff.myTurn = True
                updateGet(board)
                Stuff.toRead.setRead(0)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                Stuff.on = False
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and pause == 0:
                if Stuff.state == 1 and Stuff.myTurn is True:
                    if board.click():
                        updateSend(board)
                        if Stuff.hasServer is True:
                            Stuff.toRead.setRead(2)
                        else:
                            Stuff.toRead.setRead(1)
                    if resButton.isMouseOn():
                        board.fullReset()
                        Stuff.myTurn = False
                        Stuff.queue.put('Reset')
                        if Stuff.hasServer is True:
                            Stuff.toRead.setRead(2)
                        else:
                            Stuff.toRead.setRead(1)
        if pause > 0:
            pause = pause - 1
        elif Stuff.state == 1 and pause == 0:
            win = board.checkWin()
            if win == 1:
                pause = 50
                Stuff.score1 = Stuff.score1 + 1
                board.reset()
                winText = font.render("Player 1 Wins!", 1, colors['black'])
            elif win == 2:
                pause = 50
                board.reset()
                winText = font.render("Player 2 Wins!", 1, colors['black'])
                Stuff.score2 = Stuff.score2 + 1
            elif win == -1:
                pause = 50
                board.reset()
                winText = font.render("Draw!", 1, colors['black'])
        drawWindow()
    pygame.quit()


class QueueManager(BaseManager):
    pass


def run_forever():
    if Stuff.hasServer is True:
        return
    Stuff.queue = mp.Queue()
    Stuff.toRead = read()
    QueueManager.register('get_queue', callable=lambda: Stuff.queue)
    QueueManager.register('read', callable=lambda: Stuff.toRead)
    Stuff.manager = QueueManager(address=('localhost', 50000),
                                 authkey=b'your_secret_key')
    server = Stuff.manager.get_server()
    Stuff.hasServer = True
    Stuff.myTurn = True
    print("Server")
    server.serve_forever()


def client():
    QueueManager.register('get_queue')
    QueueManager.register('read')
    Stuff.manager = QueueManager(address=('localhost', 50000),
                                 authkey=b'your_secret_key')
    Stuff.manager.connect()
    Stuff.queue = Stuff.manager.get_queue()
    Stuff.toRead = Stuff.manager.read()
    print("Client")
    Stuff.queue.put('Connected')


def try_connect():
    try:
        client()
    except:
        try:
            run_forever()
        except:
            print("Greu cu serverele astea")


if __name__ == '__main__':
    pygame.init()  # turn all of pygame on.
    window_dim = (450, 450)
    window = pygame.display.set_mode(window_dim)
    pygame.display.set_caption('TicTacToe')
    font = pygame.font.SysFont('berlinsansfb', 35)
    playFont = pygame.font.SysFont('ariel', 150)
    # queue.put([])
    client_thread = threading.Thread(target=try_connect,daemon=True)
    client_thread.start()
    game_thread = threading.Thread(target=main)
    game_thread.start()
    game_thread.join()
    sys.exit()
