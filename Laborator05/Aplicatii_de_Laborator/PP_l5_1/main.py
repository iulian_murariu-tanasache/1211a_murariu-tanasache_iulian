from tkinter import *
from tkinter.ttk import *


class View(Frame):

    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        label1 = Label(self, text="List of integers:")
        label1.grid(row=0, column=0, sticky=(N + W))
        self.columnconfigure(1, weight=1)
        e = Entry(self, width=400)
        e.grid(row=0, column=1, sticky=W)
        labelRes = Text(self, width=400, height=50)
        labelRes.insert(END, "Result")
        labelRes.grid(row=1, column=1, sticky=N)
        listString = []

        def add():
            listString = e.get().split(",")
            print(listString)
            labelRes.delete("0.0", END)
            labelRes.insert("0.0", e.get())

        addButton = Button(self, text="Add", command=add)
        addButton.grid(row=0, column=2, sticky=(N + E))

        toShow = ""

        def filterOdd():
            for i in listString:
                if i % 2 == 1:
                    listString.remove(i)
                else:
                    toShow = toShow + i + ","
            labelRes.delete("0.0", END)
            labelRes.insert("0.0", toShow)

        filter = Button(self, text="Filter odd", command=filterOdd)
        filter.grid(row=1, column=2, sticky=E)


class Application(Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("Aplicatia 1")
        self.geometry("700x500")
        self.resizable(width=False, height=False)
        View(self).grid(sticky=(N + E + S + W))
        self.columnconfigure(0, weight=1)


app = Application()
app.mainloop()
