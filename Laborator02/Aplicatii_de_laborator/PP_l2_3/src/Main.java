//import libraria principala polyglot din graalvm
import org.graalvm.polyglot.*;

import java.util.stream.Stream;

//clasa principala - aplicatie JAVA
class Polyglot {
    private static int[] generare_lista()
    {
        Context polyglot = Context.create();
        Value array = polyglot.eval("python","[random.range(0,99) for i in range(0,20)]");
        String str = array.asString();
        int[] arr = Stream.of(str).mapToInt(Integer::parseInt).toArray();
        polyglot.close();
        return arr;
    }

    private static void afisare(int[] list)
    {
        Context polyglot = Context.create();
        polyglot.eval("js","console.log(" +list+")");
        polyglot.close();
    }

    private static void sortare(int[] list)
    {
        Context polyglot = Context.create();
        polyglot.eval("R","sort(" + list + ", decreasing = FALSE)");
        polyglot.close();
    }

    //functia MAIN
    public static void main(String[] args) {
        //construim un context pentru evaluare elemente JS
        Context polyglot = Context.create();
        int[] a = generare_lista();
        afisare(a);
       sortare(a);
        // inchidem contextul Polyglot
        polyglot.close();
    }
}

