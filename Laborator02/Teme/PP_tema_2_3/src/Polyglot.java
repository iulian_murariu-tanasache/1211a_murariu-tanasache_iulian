import org.graalvm.polyglot.*;

class Polyglot {
    private static int[] PYinput(){
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        int[] res = new int [2] ;
        Value in = polyglot.eval("python","int(input('nr = '))");
        res[0] = in.asInt();
        in = polyglot.eval("python","int(input('x = '))");
        res[1] = in.asInt();
        polyglot.close();
        return res;
    }

    private static void RBinomiala(int n, int x){
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        double res;
        Value prob = polyglot.eval("R","pbinom(" + x + "," + n + ", 0.5)");
        res = prob.asDouble();
        polyglot.close();
        System.out.println("Probabilitatea este: " + res);
    }

    public static void main(String[] args) {
        Context polyglot = Context.create();
        int[] citite = PYinput();
        RBinomiala(citite[0],citite[1]);
        polyglot.close();
    }
}
