import java.io.*
import java.util.*

class notita(private var author: String, private var name : String, private var text : String, private var data: String, private var ora : String)
{
    fun getAuthor() = author
    fun getText() = text
    fun getName() = name
    fun getData() = data
    fun getOra() = ora

    override fun toString() = "Author: ${author}\nName: ${name}\nDate: ${data}\nTime: ${ora}\n${text}"
}

class Manager()
{
    private var lista = mutableListOf<notita>()

    fun add(n : notita)
    {
        lista.add(n)

        val a = File("")
        File( a.absolutePath,"/notite/${n.getName()}.txt").createNewFile()
        File(a.absolutePath,"/notite/${n.getName()}.txt").printWriter().use{ out ->
            out.print(n.toString())
        }
    }

    fun create() : notita
    {
        val br = BufferedReader(InputStreamReader(System.`in`));
        print("Nume: ")
        val name = br.readLine()
        print("Autor: ")
        val author = br.readLine()
        print("Data: ")
        val data = br.readLine()
        print("Ora: ")
        val ora = br.readLine()
        var content = ""
        println("Content(\":q\" scris pe o noua linie -> termina scrierea): ")
        var line = br.readLine()
        while(line != ":q")
        {
            content += line + "\n"
            line = br.readLine()
        }
        val n = notita(author,name,content,data,ora)
        return n
    }

    fun get(n : String) : notita?
    {
        if(lista.size == 0)
        {
            println("Lista goala!")
            return null
        }
        lista.forEach {
            if(it.getName() == n)
                return it
        }
        return null
    }

    fun load()
    {

        val a = File("")
        File(a.absolutePath,"/notite/").walk(FileWalkDirection.TOP_DOWN).forEach { file->
            if(file.isFile) {
                var a = ""
                var n = ""
                var d = ""
                var t = ""
                var c = ""
                File(file.absolutePath).forEachLine {

                    if (it.contains("Author: ")) {
                        a = it.substringAfterLast(" ")
                    } else if (it.contains("Name: ")) {
                        n = it.substringAfterLast(" ")
                    } else if (it.contains("Date: ")) {
                        d = it.substringAfterLast(" ")
                    } else if (it.contains("Time: ")) {
                        t = it.substringAfterLast(" ")
                    } else {
                        c += it
                    }
                }
                lista.add(notita(a, n, c, d, t))
            }
        }
    }

    fun delete(name : String)
    {
        if(lista.size == 0)
        {
            println("Lista goala!")
            return
        }
        run done@{
            val a = File("")
            File(a.absolutePath,"/notite/").walk(FileWalkDirection.TOP_DOWN).forEach { file->

                if (file.isFile && file.nameWithoutExtension == name) {
                    file.delete()
                    return@done
                }
            }
        }
        run altdone@{
            lista.forEach {
                if(it.getName() == name)
                {
                    lista.remove(it)
                    return@altdone
                }
            }
        }
    }

    fun afisare()
    {
        if(lista.size == 0)
        {
            println("Lista goala!")
            return
        }
        lista.forEach {
            println("Author: ${it.getAuthor()}; Name: ${it.getName()}; Date: ${it.getData()};T ime: ${it.getOra()}")
        }
    }
}

class Menu(private var manager : Manager){

    fun afis()
    {
        println("---Notite Manager.Version: The Best---")
        while(true) {
            println()
            println("1) Afisare lista de notite")
            println("2) Creeare notita noua")
            println("3) Stergere notita dupa nume")
            println("4) Afisare anumita notita")
            println("else) Exit")
            print("Alege: ")
            val input = Scanner(System.`in`)
            val opt = input.nextInt()
            when (opt) {
                1 -> manager.afisare()
                2 -> manager.add(manager.create())
                3 -> manager.delete(input.next())
                4 -> println(manager?.get(input.next()).toString())
                else -> return
            }
        }
    }
}


fun main(args: Array<String>)
{
    val manager = Manager()
    manager.load()
    val meniu = Menu(manager)
    meniu.afis()
}