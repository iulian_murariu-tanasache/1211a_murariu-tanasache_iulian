import sun.security.ec.point.ProjectivePoint
import java.io.File

class Content(private var author: String, private var text : String, private var name : String, private var publisher : String)
{
    fun getAuthor() = author
    fun getText() = text
    fun getName() = name
    fun getPublisher() = publisher
    fun setAuthor(a : String){author = a}
    fun setText(t : String){text = t}
    fun setName(n : String){name = n}
    fun setPublisher(p : String){publisher = p}

}

class Book(private var data:Content)
{
    override fun toString() = "Author: ${data.getAuthor()}\nName: ${data.getName()}\nPublisher: ${data.getPublisher()}\n${data.getText()}"
    fun getAuthor() = data.getAuthor()
    fun getText() = data.getText()
    fun getName() = data.getName()
    fun getPublisher() = data.getPublisher()
    fun hasAuthor(a : String) = getAuthor() == a
    fun hasTitle(t : String) = getName() == t
    fun isPublishedBy(p : String) = getPublisher() == p
}

class Library(private var books : MutableSet<Book>)
{
    fun getBooks() = books
    fun addBook(b : Book){books.add(b)}
    fun findAllByAuthor(a : String) : Set<Book>{
        var lista : MutableSet<Book> = mutableSetOf()
        books.forEach { it->
            if(it.hasAuthor(a))
                lista.add(it)
        }
        return lista
    }
    fun findAllByName(n : String) : Set<Book>{
        var lista : MutableSet<Book> = mutableSetOf()
        books.forEach { it->
            if(it.hasTitle(n))
                lista.add(it)
        }
        return lista
    }
    fun findAllByPublisher(p : String) : Set<Book>{
        var lista : MutableSet<Book> = mutableSetOf()
        books.forEach { it->
            if(it.isPublishedBy(p))
                lista.add(it)
        }
        return lista
    }
}

interface printRaw{
    fun printBooksRaw(set : Set<Book>){
        File("rawBooks.txt").writeText("")
        set.forEach { it->
            File("rawBooks.txt").appendText(it.toString() + "\n")
        }
    }
}

interface printHTML{
    fun printHTML(set : Set<Book>){
        File("htmlBooks.html").writeText("<library>\n")
        set.forEach { it->
            File("htmlBooks.html").appendText("<book>\n")
            File("htmlBooks.html").appendText(" <title>${it.getName()}</title>\n")
            File("htmlBooks.html").appendText(" <author>${it.getAuthor()}</author>\n")
            File("htmlBooks.html").appendText(" <publisher>${it.getPublisher()}</publisher>\n")
            File("htmlBooks.html").appendText(" <text>${it.getText()}</text>\n")
            File("htmlBooks.html").appendText("</book>\n")
        }
        File("htmlBooks.html").appendText("</library>\n")
    }
}

interface printJSON{
    fun printJSON(set : Set<Book>){
        File("jsonBooks.JSON").writeText("\"library:\"")
        set.forEach { it->
            File("jsonBooks.JSON").appendText("\"\tbook:\"")
        }
    }
}

class LibraryPrinter : printRaw, printHTML, printJSON
{}

fun main(args: Array<String>)
{
    var b = Book(Content("Eu","Acum lucrez la PP","Ceea ce fac","Tot eu"))
    var lib = Library(mutableSetOf(b))
    b = Book(Content("Eu","Si altii lucreaza la PP, cred","Cu toti facem","Tot eu"))
    lib.addBook(b)
    b = Book(Content("Altcineva","Si altii lucreaza la PP, cu siguranta","Cu toti facem","altcineva"))
    lib.addBook(b)
    val printer = LibraryPrinter()
    printer.printBooksRaw(lib.getBooks())
    printer.printHTML(lib.getBooks())

}