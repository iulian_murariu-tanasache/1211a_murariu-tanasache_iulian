import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.util.*;

public class CalculatorExtins extends JFrame {
    JButton digits[] = {
            new JButton(" 0 "),
            new JButton(" 1 "),
            new JButton(" 2 "),
            new JButton(" 3 "),
            new JButton(" 4 "),
            new JButton(" 5 "),
            new JButton(" 6 "),
            new JButton(" 7 "),
            new JButton(" 8 "),
            new JButton(" 9 ")
    };

    JButton operators[] = {
            new JButton(" + "),
            new JButton(" - "),
            new JButton(" * "),
            new JButton(" / "),
            new JButton(" ( "),
            new JButton(" ) "),
            new JButton(" = "),
            new JButton(" C ")
    };

    String oper_values[] = {"+", "-", "*", "/", "(", ")", "=", ""};

    String value;
    char operator;
    int last = 0;

    JTextArea area = new JTextArea(3, 5);

    public static void main(String[] args) {
        CalculatorExtins calculatorExtins = new CalculatorExtins();
        calculatorExtins.setSize(250, 260);
        calculatorExtins.setTitle(" Java-Calc, PP Lab1 ");
        calculatorExtins.setResizable(false);
        calculatorExtins.setVisible(true);
        calculatorExtins.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public boolean prioritate(int cod1, int cod2)
    {
        if(cod2 == 5)
            return true;
        if(cod2 == 4 || cod1 > 5 || cod2 > 5)
            return false;
        cod1 = cod1/2 + 1;
        cod2 = cod2/2 + 1;
        if(cod2 <= cod1)
            return true;
        return false;
    }

    public CalculatorExtins(){
        Stack operatori = new Stack();
        Stack operanzi = new Stack();
        add(new JScrollPane(area), BorderLayout.NORTH);
        JPanel buttonpanel = new JPanel();
        buttonpanel.setLayout(new FlowLayout());

        for (int i=0;i<10;i++)
            buttonpanel.add(digits[i]);

        for (int i=0;i<8;i++)
            buttonpanel.add(operators[i]);

        add(buttonpanel, BorderLayout.CENTER);
        area.setForeground(Color.BLACK);
        area.setBackground(Color.WHITE);
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setEditable(false);

        for (int i=0;i<10;i++) {
            int finalI = i;
            digits[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    area.append(Integer.toString(finalI) + " ");
                }
            });
        }

        for (int i=0;i<8;i++){
            int finalI = i;
            operators[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    if (finalI == 7){
                        area.setText("");
                        operanzi.empty();
                        operatori.empty();
                        last = 0;
                    }
                    else if (finalI == 6) {
                        try {
                            double total = 0.0;
                            if(operator != ')')
                                total = Double.parseDouble(area.getText().substring(last, area.getText().length()-1));
                            else
                                total = (double)operanzi.pop();
                            while(!operatori.isEmpty())
                            {

                                switch(oper_values[(int)operatori.peek()])
                                {
                                    case "+": total = total + (double)operanzi.pop(); break;
                                    case "-": total = (double)operanzi.pop() - total; break;
                                    case "/": total = (double)operanzi.pop() / total; break;
                                    case "*": total = total * (double)operanzi.pop(); break;
                                }
                                operatori.pop();
                            }
                            area.append(" = " + total);
                        } catch (Exception e) {
                            area.setText(" !!!Probleme!!! ");
                        }
                    }
                    else {
                        area.append(oper_values[finalI] + " ");
                        char lastop = operator;
                        operator = oper_values[finalI].charAt(0);
                        String operand = "";
                        double op = 0.0;
                        if(operator != '(' && lastop != ')')
                        {
                            operand = area.getText().substring(last, area.getText().lastIndexOf(operator) - 1);
                            op = Double.parseDouble(operand);

                        }
                        else if(lastop == ')' && operator == ')')
                            op = (double)operanzi.pop();
                        last = area.getText().lastIndexOf(operator) + 2;
                        if(!operatori.isEmpty() && prioritate((int)operatori.peek(),finalI))
                        {
                            switch(oper_values[(int)operatori.pop()])
                            {
                                case "+": op = op + (double)operanzi.pop(); break;
                                case "-": op = (double)operanzi.pop() - op; break;
                                case "/": op = (double)operanzi.pop() / op; break;
                                case "*": op = op * (double)operanzi.pop(); break;
                                case "(": operatori.push(4);break;
                            }
                            if(operator == ')')
                            {
                                while(oper_values[(int)operatori.peek()] != "(")
                                    switch(oper_values[(int)operatori.pop()])
                                    {
                                        case "+": op = op + (double)operanzi.pop(); break;
                                        case "-": op = (double)operanzi.pop() - op; break;
                                        case "/": op = (double)operanzi.pop() / op; break;
                                        case "*": op = op * (double)operanzi.pop(); break;
                                        case "(": operatori.push(4);break;
                                    }
                                if(!operatori.isEmpty()&&(int)operatori.peek() == 4)
                                    operatori.pop();
                            }
                        }
                        if(operator != ')')
                            operatori.push(finalI);
                        if((operator != '(' && lastop != ')') || operator == ')')
                            operanzi.push(op);

                    }
                }
            });
        }
    }
}