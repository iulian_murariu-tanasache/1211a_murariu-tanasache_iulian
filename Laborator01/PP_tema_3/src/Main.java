import java.io.IOException;
import java.nio.file.*;

public class Main {

    public static void main(String[] args) throws IOException {
        Path filepath = Path.of("in.txt");
        String content = "";
        int k = 0;
        content = new String(Files.readAllBytes(filepath));
        char[] ch = new char[content.length()];
        ch = content.toCharArray();
        char[] newch = new char[content.length()];
        char last = 'a';
        for (int i = 0; i < ch.length; ++i) {
            if (!(ch[i] == '.' || ch[i] == '?' || ch[i] == '!' || ch[i] == ';' || ch[i] == ':' || ch[i] == '\"' || ch[i] == ',' || (i > 0 && ch[i] == ' ' && last == ' ') || (ch[i] >= '0' && ch[i] <= '9'))) {
                newch[k] = ch[i];
                last = newch[k];
                k++;
            }
        }
        System.out.println(ch);
        System.out.println(newch);

    }

}
