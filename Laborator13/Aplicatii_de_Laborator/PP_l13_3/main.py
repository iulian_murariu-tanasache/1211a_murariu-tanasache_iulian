def zip(*args):
    length = min(map(len, args))
    zipped = []
    for i in range(0, length):
        zipped.append(tuple(list(map(lambda l: l[i], args))))
    return zipped


if __name__ == '__main__':
    list = zip(['a', 'b', 'c'], [2, 3])
    print(list)
