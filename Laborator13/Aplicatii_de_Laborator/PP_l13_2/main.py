class String(str):
    def toPascalCase(self):
        return ''.join(map(lambda s: s.capitalize(), self.split(' ')))


if __name__ == '__main__':
    print(String(input('Introduceti un string: ')).toPascalCase())
