import types
import math

def decorator_cu_argumente(before_fct, after_fct, replacement_fct=None):
    def wrap(f):
        def wrapped_f(*args, **kwargs):
            if isinstance(before_fct, types.FunctionType):
                before_fct()  # execute before function
            if replacement_fct and isinstance(replacement_fct, types.FunctionType):
                output = replacement_fct(*args, **kwargs)
            else:
                output = f(*args, **kwargs)
            if isinstance(after_fct, types.FunctionType):
                after_fct()  # execute after function
            return output

        return wrapped_f

    return wrap


def before():
    print("Before")


def after():
    print("After")


def replacement(*args, **kwargs):
    print("Replacement")
    return "replaced"


@decorator_cu_argumente(before_fct=before, after_fct=after, replacement_fct=replacement)
def func0(name, question):
    print("func0")
    return f"Hello {name}, {question}"


@decorator_cu_argumente(before_fct=before, after_fct=after)
def func1(name):
    print("func1")
    return f"Hello {name}"


def isPrime(number):
    if number < 2:
        return False
    elif number == 2:
        return True
    for i in range(2, int(math.sqrt(number)) + 1):
        if number % i == 0:
            return False
    return True


class int(int):
    @decorator_cu_argumente(before_fct=before, after_fct=after, replacement_fct=isPrime)
    def deDecorat(self):
        pass


if __name__ == '__main__':
    # print("Returned:", func0("Ion Popescu", "how are you?"))
    # print("Returned:", func1("Ion Popescu"))
    print(int(input('Introduceti un numar: ')).deDecorat())
